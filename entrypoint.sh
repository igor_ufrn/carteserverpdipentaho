#!/bin/sh
echo 'executando ENTRYPOINT com usuário:' $(whoami)
echo 'Realizando o parse das configurações do Carte'
envsubst < carte-config.xml > carte-config-parsed.xml

if [ "$MASTER" = 'N' ] ; then
    echo 'Esperando inicialização do Carte Master...'
    sleep 15
fi


echo 'Iniciando o Carte'
./carte.sh carte-config-parsed.xml
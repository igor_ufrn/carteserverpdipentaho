Este demonstra como containerizar a ferramenta de ETL Pentaho,
de modo que o processamento dos JOBS possam ser distribuídos 
e paralelizados. Os nós do cluster se comunicam entre si
e executam o trabalho. Os nós podem estar em qualquer 
ambiente, independentemente um do outro: nuvem, on-premise, na sua casa.
